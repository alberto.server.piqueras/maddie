import { Component, OnInit } from '@angular/core';
import { noteI } from '../../models/note/note';
import { TouchSequence } from 'selenium-webdriver';
import { saveAs } from 'file-saver';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

declare var electron: any;

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  isToggled = false;
  notes = [];
  title = '';
  text = '';
  isDark = true;
  hour: any;
  minute: any;
  currentTime: Date = new Date();
  creatingNote = false;
  contextmenu = false;
  contextmenuX = 0;
  contextmenuY = 0;
  isImmersive = false;

  private currentNote = 0;
  private clickedNote = 0;



  constructor() {

    setInterval(() => {
      this.currentTime = new Date();
      this.hour = this.currentTime.getHours();
      this.minute = this.currentTime.getMinutes();
    }, 1);

  }

  ngOnInit() {

    if (localStorage.getItem('notes') != null) {
      this.setNotes();
    }

    if (localStorage.getItem('theme') != null) {
      this.loadTheme();
    }

  }

  loadNote(note: noteI): void {

    this.title = note.title;
    this.text = note.text;
    this.currentNote = note.id;

  }

  saveNote(): void {

    const txtTitle = (<HTMLInputElement>document.getElementById('title')).value;
    const txtText = (<HTMLTextAreaElement>document.getElementById('text')).value;

    // If title is filled then continue
    if (txtTitle !== '') {

      if ( (localStorage.getItem('notes') != null ) ) {

        if ( JSON.parse(localStorage.getItem('notes')).length !== 0) {

          // UPDATE A NOTE
          if (this.notes[this.currentNote].title === txtTitle) {

            const note: noteI = {
              id: this.notes[this.currentNote].id,
              title: this.notes[this.currentNote].title,
              text: txtText,
              isFile: this.notes[this.currentNote].isFile
            };

            if (!note.isFile) {
              this.notes[this.currentNote] = note;
              localStorage.setItem('notes', JSON.stringify(this.notes));
            } else {
              const noteFile = new File([note.text], note.title, {type: 'text/plain;charset=utf-8'});
              saveAs(noteFile);
            }

          } else this.saveNewNote();
        } else this.saveNewNote();
      } else this.saveNewNote();
    }

  }

  newNote(): void {

    const form = document.getElementById('form');

    this.title = null;
    this.title = '';
    this.text = null;
    this.text = '';

  }

  setNotes(): void {
    this.notes = JSON.parse(localStorage.getItem('notes'));
  }

  deleteNote(i): void {
    const msg = confirm('Are you sure you wanna delete this note?');
    let txtTitle = (<HTMLInputElement>document.getElementById('title'));
    let txtText = (<HTMLTextAreaElement>document.getElementById('text'));
    if (msg) {
      this.notes.splice(i, 1);
      localStorage.removeItem('notes');
      localStorage.setItem('notes', JSON.stringify(this.notes));
      txtText.value = '';
      txtTitle.value = '';
    }
  }

  changeTheme(): void {

    if (this.isDark) {
      this.isDark = false;
      localStorage.setItem('theme', 'light');
    } else {
      this.isDark = true;
      localStorage.setItem('theme', 'dark');
    }

  }

  loadTheme(): void {

    if (localStorage.getItem('theme') === 'light') {
      this.isDark = false;
    } else {
      this.isDark = true;
    }

  }

  createNote(): void {
    if (this.creatingNote) this.creatingNote = false;
    else this.creatingNote = true;
  }

  onChange(event: any): void {

    if (event.target.files.length > 0) {

      const file: File = event.target.files[0];
      const reader: FileReader = new FileReader();
      // tslint:disable-next-line: only-arrow-functions
      reader.onload = (e) => {
        const newNote: noteI = { id: this.notes.length, title: event.target.files[0].name, text: reader.result, isFile: true };
        this.notes.push(newNote);
        localStorage.setItem('notes', JSON.stringify(this.notes));
      };
      reader.readAsText(file);
    }
  }

  exportNote(): void {
    const noteFile = new File([this.notes[this.clickedNote].text], this.notes[this.clickedNote].title, {type: 'text/plain;charset=utf-8'});
    saveAs(noteFile);
  }




  /* =========== CONTEXT MENU =========== */

  // activates the menu with the coordinates
  onrightClick(event, i): void {
    this.contextmenuX = event.clientX;
    this.contextmenuY = event.clientY;
    this.contextmenu = true;
    this.clickedNote = i;
  }

  // disables the menu
  disableContextMenu(): void {
    this.contextmenu = false;
  }

  setImmersive(): void {
    if (this.isImmersive) this.isImmersive = false;
    else this.isImmersive = true;
  }


  /* =========== PRIVATE METHODS =========== */

  // SAVE NEW NOTE
  private saveNewNote(): void {

    const txtTitle = (<HTMLInputElement>document.getElementById('title')).value;
    const txtText = (<HTMLTextAreaElement>document.getElementById('text')).value;
    const newNote: noteI = { id: this.notes.length, title: txtTitle, text: txtText, isFile: false };

    this.notes.push(newNote);
    localStorage.setItem('notes', JSON.stringify(this.notes));

  }

}
