# Maddie
![Maddie Banner](https://i.lensdump.com/i/iWHM8A.jpg)
Hello there! This is **Maddie**, an **immersive minimalist text editor** based on **notes/document creation.**

Without distractions you will work more efficiently thanks to the **immersive mode!**

## Features
- **Create documents:** You can create a document from zero and then export to a file saving it where you desire.
- **Open a file:** If you want to open a text file, format doesn't matter, and you want to edit... Maddie will do it without problems!
- **Immersive Mode:** Do you need no distractions for a more productive writting? Sit well on your chair, breath deeply and enable the immersive mode, and remove any remaining elements from the screen!

## Install & run
In this **[page](https://arcalie-rodriguez.itch.io/maddie)** you will find the **installers** for Windows, Linux & macOS up to date.

Or if you prefer **clone this project** to your computer and run it, simply use theese commands:

`git clone https://gitlab.com/alberto.server.piqueras/maddie.git`

`npm i`

`npm run electron`


## Extras
- You can follow me on my **[Twitter](https://twitter.com/ArcalieRdgz)** for more information about Maddie and future projects
- This is the **[License](https://gitlab.com/alberto.server.piqueras/maddie/blob/master/LICENSE)** file for the license rights and limitations **[MIT]**
- If you want to support me and this project you can do it **[here](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=aspval99@gmail.com)**
